package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car {
    private int maxSpeed;


    public SportCar(SportCarBuilder sportCarBuilder) {
        //super(carBrand, carClass, carWeight, driver, motor);
        this.maxSpeed = sportCarBuilder.getMaxSpeed();
    }



//    public void setMaxSpeed(int maxSpeed) {
//        this.maxSpeed = maxSpeed;
//    }



    public String toString() {
        return super.toString() + "max speed - " + maxSpeed;
    }

    public static class SportCarBuilder extends CarBuilder
    {
        private int maxSpeed;
        public Driver driver;
        public Engine engine;
        public SportCarBuilder carDriver(Driver driver)
        {
            this.driver = driver;
            return this;
        }
        public SportCarBuilder carEngine(Engine engine)
        {
            this.engine = engine;
            return this;
        }

        public int getMaxSpeed() {
            return maxSpeed;
        }
        public SportCarBuilder maxSpeed(int maxSpeed)
        {
            this.maxSpeed = maxSpeed;
            return this;
        }

        public SportCar build()
        {
            return new SportCar(this);
        }



    }



}
