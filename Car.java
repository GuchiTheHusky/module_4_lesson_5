package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Car {

    protected String carBrand;
    private String carClass;
    private int carWeight;

    private Driver driver;
    private Engine engine;

    private Car(CarBuilder carBuilder) {
        this.carBrand = carBuilder.getCarBrand();
        this.carClass = carBuilder.getCarClass();
        this.carWeight = carBuilder.carWeight;
        this.driver = carBuilder.driver;
        this.engine = carBuilder.engine;
    }

    public Car() {
    }

    public void start() {
        System.out.println("Let's go");
    }

    public void stop() {
        System.out.println("Stop");
    }

    public void turnLeft() {
        System.out.println("Turn left");
    }

    public void turnRight() {
        System.out.println("Turn right");
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public void setMotor(Engine motor) {
        this.engine = motor;
    }

    public String toString() {
        return "Car brand - " + carBrand + "; car class - "
                + carClass + "; car weight - " + carWeight
                + ";" + "\n" + "driver - " + driver + ";"
                + "\n" + "motor - " + engine;
    }

    public static class CarBuilder
    {
        public String carBrand;
        private String carClass;
        private int carWeight;

        public Driver driver;
        public Engine engine;


        public CarBuilder carDriver(Driver driver)
        {
            this.driver = driver;
            return this;
        }
        public CarBuilder carEngine(Engine engine)
        {
            this.engine = engine;
            return this;
        }

        public String getCarBrand() {
            return carBrand;
        }

        public String getCarClass() {
            return carClass;
        }

        public int getWeight() {
            return carWeight;
        }

        public CarBuilder carBrand(String carBrand) {
            this.carBrand = carBrand;
            return this;
        }

        public CarBuilder carClass(String carClass) {
            this.carClass = carClass;
            return this;
        }

        public CarBuilder carWeight(int carWeight) {
            this.carWeight = carWeight;
            return this;
        }

        public Car build()
        {
            return new Car(this);
        }



    }





}
