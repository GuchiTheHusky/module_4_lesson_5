package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;


public class Main {
    public static void main(String[] args) {
        Driver firstDriver = new Driver.DriverBuilder()
                                       .fullName("Bob")
                                       .gender("male")
                                       .age(31)
                                       .phoneNumber(976976976)
                                       .drivingExperience(13).build();

        Engine firstEngine = new Engine.EngineBuilder()
                                       .carProducer("BMW")
                                       .carPower(3000).build();
        Car car = new Car.CarBuilder()
                         .carBrand("KIA")
                         .carClass("B")
                         .carWeight(1800)
                         .carDriver(firstDriver)
                         .carEngine(firstEngine).build();

        Driver secondDriver = new Driver.DriverBuilder()
                                        .fullName("Charlie")
                                        .gender("male")
                                        .age(25)
                                        .phoneNumber(103)
                                        .drivingExperience(9).build();

        Engine secondEngine = new Engine.EngineBuilder()
                                        .carProducer("Mercedes")
                                        .carPower(2500).build();
        Lorry lorryCar = new Lorry.LorryBuilder()
                                  .carBrand("Iveco")
                                  .carClass("E")
                                  .carWeight(3500)
                                  .carDriver(secondDriver)
                                  .carEngine(secondEngine)
                                  .lorryCarringCapacity(4000).build();

        Driver thirdDriver = new Driver.DriverBuilder()
                                       .fullName("Alice")
                                       .gender("female")
                                       .age(24)
                                       .phoneNumber(111)
                                       .drivingExperience(5).build();
        Engine thirdEngine = new Engine.EngineBuilder()
                                       .carProducer("Audi")
                                       .carPower(2200).build();
        SportCar sportCar = new SportCar.SportCarBuilder()
                                        .carBrand("Nissan")
                                        .carClass("C")
                                        .carWeight(1400)
                                        .carDriver(thirdDriver)
                                        .carEngine(thirdEngine)
                                        .maxSpeed(250).build();

        System.out.println(car + "\n" + lorryCar + "\n" + sportCar);





    }

}
