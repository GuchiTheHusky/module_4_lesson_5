package com.company.entities;

import com.company.professions.Driver;

public class Person {
    private String fullName;
    private String gender;
    private int age;
    private int phoneNumber;

    public Person(PersonBuilder personBuilder) {
        this.fullName = personBuilder.fullName;
        this.gender = personBuilder.gender;
        this.age = personBuilder.age;
        this.phoneNumber = personBuilder.phoneNumber;
    }

    public Person() {
    }

    public String toString() {
        return fullName + "; gender - " + gender + "; age - " + age + "; phone number - " + phoneNumber;
    }

    public static class PersonBuilder
    {
        private String fullName;
        private String gender;
        private int age;
        private int phoneNumber;

        public String getFullName() {
            return fullName;
        }

        public String getGender() {
            return gender;
        }

        public int getAge() {
            return age;
        }

        public int getPhoneNumber() {
            return phoneNumber;
        }

        public PersonBuilder fullName(String fullName) {
            this.fullName = fullName;
            return this;
        }

        public PersonBuilder gender(String gender) {
            this.gender = gender;
            return this;
        }

        public PersonBuilder age(int age) {
            this.age = age;
            return this;
        }

        public PersonBuilder phoneNumber(int phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Person build()
        {
            return new Person(this);
        }






    }
}
