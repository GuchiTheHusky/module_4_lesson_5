package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car {

    private int carringCapacity;
    private Lorry(LorryBuilder lorryBuilder) {
//        super(carBrand, carClass, carWeight, driver, motor);
        this.carringCapacity = lorryBuilder.getCarringCapacity();
    }

    public String toString() {
        return super.toString() + " carring capacity - " + carringCapacity;
    }

    public static class LorryBuilder extends CarBuilder
    {
        private int carringCapacity;
        public Driver driver;
        public Engine engine;
        public LorryBuilder carDriver(Driver driver)
        {
            this.driver = driver;
            return this;
        }
        public LorryBuilder carEngine(Engine engine)
        {
            this.engine = engine;
            return this;
        }

        public int getCarringCapacity() {
            return carringCapacity;
        }

        public LorryBuilder lorryCarringCapacity(int carringCapacity)
        {
            this.carringCapacity = carringCapacity;
            return this;
        }
        public Lorry build()
        {
            return new Lorry(this);
        }



    }








}
