package com.company.details;
public class Engine {
    private String carProducer;
    private int carPower;

    private Engine(EngineBuilder engineBuilder) {
        this.carProducer = engineBuilder.getCarProducer();
        this.carPower = engineBuilder.getCarPower();

    }

    public String toString() {
        return "car producer " + carProducer + "; car power - " + carPower;
    }

    public static class EngineBuilder{
        private String carProducer;
        private int carPower;

        private String getCarProducer() {
            return carProducer;
        }

        private int getCarPower() {
            return carPower;
        }
        public EngineBuilder carProducer(String carProducer)
        {
            this.carProducer = carProducer;
            return this;
        }
        public EngineBuilder carPower(int carPower)
        {
            this.carPower = carPower;
            return this;
        }
        public Engine build()
        {
            return new Engine(this);
        }



    }

}

