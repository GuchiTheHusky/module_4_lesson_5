package com.company.professions;

import com.company.entities.Person;

public class Driver extends Person {
    private int drivingExperience;

    public Driver(DriverBuilder driverBuilder) {
        //super(fullName, gender, age, phoneNumber, drivingExperience);
        this.drivingExperience = driverBuilder.getDrivingExperience();
    }

    public String toString() {
        return super.toString() + "; drivingExperience - " + drivingExperience;
    }

    public static class DriverBuilder extends PersonBuilder
    {
        private int drivingExperience;

        public int getDrivingExperience() {
            return drivingExperience;
        }
        public DriverBuilder drivingExperience(int drivingExperience) {
            this.drivingExperience = drivingExperience;
            return this;
        }
        public Driver build ()
        {
            return new Driver(this);
        }


    }

}